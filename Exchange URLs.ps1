﻿
#BaseURL
$BaseURL = "https://webmail.linktechconsulting.com"
$UpdateServer = ""

#Set Active Sync
$EASURL = $BaseURL + "/Microsoft-Server-ActiveSync"
Get-ActiveSyncVirtualDirectory -server $UpdateServer | Set-ActiveSyncVirtualDirectory -InternalUrl $EASURL -ExternalUrl $EASURL

#Set Autodiscover
$AutodiscoverURL = "autodiscover.linktechconsulting.com/AutoDiscover/AutoDiscover.xml"
Set-ClientAccessServer -Identity $UpdateServer -AutoDiscoverServiceInternalUri $AutodiscoverURL

#Set ECP
$ECPUrl = $BaseURL + "/ecp"
Get-EcpVirtualDirectory -Server $UpdateServer | Set-EcpVirtualDirectory -InternalUrl $ECPUrl -ExternalUrl $ECPUrl

#Set MAPI
$MapiURL = $BaseURL + "/mapi"
Get-MapiVirtualDirectory -Server $UpdateServer | Set-MapiVirtualDirectory -InternalUrl $MapiURL -ExternalUrl $MapiURL

#Set OAB
$OABURL = $BaseURL + "/OAB"
Get-OabVirtualDirectory -server $UpdateServer | Set-OabVirtualDirectory -InternalUrl $OABURL -ExternalUrl $OABURL

#Set Outlook Anywhere
$OAHostname = "webmail.linktechconsulting.com"
Get-OutlookAnywhere -Server $UpdateServer | Set-OutlookAnywhere -InternalHostname $OAHostname -ExternalHostname $OAHostname -ExternalClientAuthenticationMethod Ntlm -InternalClientAuthenticationMethod Ntlm -IISAuthenticationMethods "{Basic, Ntlm, Negotiate}"

#Set OWA
$OWAUrl = $BaseURL + "/OWA"
Get-OwaVirtualDirectory -Server $UpdateServer | Set-OwaVirtualDirectory -ExternalUrl $OWAUrl -InternalUrl $OWAUrl

#Set EWS
$EWSUrl = $BaseURL + "/EWS/Exchange.asmx"
Get-WebServicesVirtualDirectory -Server $UpdateServer | Set-WebServicesVirtualDirectory -InternalUrl $EWSUrl -ExternalUrl $EWSUrl



###Verify you didn't fuck up

#ACTIVESYNC (EAS)
Get-ActiveSyncVirtualDirectory -server $UpdateServer | fl Identity, *ternalurl*
#AUTODISCOVER
 Get-ClientAccessServer -Identity $UpdateServer | fl Identity, *ternaluri*
#EXCHANGE CONTROL PANEL (ECP)
 Get-EcpVirtualDirectory -server $UpdateServer | fl Identity, *ternalurl*
#MAPI...Exchange 2013 and newer
 Get-MapiVirtualDirectory -server $UpdateServer| fl Identity, *ternalurl*
#OFFLINE ADDRESS BOOK (OAB)
 Get-OabVirtualDirectory -server $UpdateServer | fl Identity, *ternalurl*
#OUTLOOK ANYWHERE (OA)
 Get-OutlookAnywhere -server $UpdateServer | fl Identity, *ternalhost*, *ticationmeth*
#OUTLOOK WEB APP (OWA)
 Get-OwaVirtualDirectory -server $UpdateServer | fl Identity, *ternalurl*
#EXCHANGE WEB SERVICES (EWS)
Get-WebServicesVirtualDirectory -server $UpdateServer | fl Identity, *ternalurl*