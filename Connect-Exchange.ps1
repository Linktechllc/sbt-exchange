Function Connect-Exchange {
 
    param(
        [Parameter( Mandatory=$false)]
        [string]$URL="mail3.sbt.local"
    )
    
    #$Credentials = Get-Credential -Message "Enter your Exchange admin credentials"
 
    $ExOPSession = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri http://$URL/PowerShell/
 
    Import-PSSession $ExOPSession
 
}


Connect-Exchange


#
#ACTIVESYNC (EAS)
Get-ExchangeServer | Get-ActiveSyncVirtualDirectory | fl Identity, *ternalurl*
#AUTODISCOVER
Get-ExchangeServer | Get-ClientAccessServer | fl Identity, *ternaluri*
#EXCHANGE CONTROL PANEL (ECP)
Get-ExchangeServer | Get-EcpVirtualDirectory | fl Identity, *ternalurl*
#MAPI...Exchange 2013 and newer
Get-ExchangeServer | Get-MapiVirtualDirectory | fl Identity, *ternalurl*
#OFFLINE ADDRESS BOOK (OAB)
Get-ExchangeServer | Get-OabVirtualDirectory | fl Identity, *ternalurl*
#OUTLOOK ANYWHERE (OA)
Get-ExchangeServer | Get-OutlookAnywhere | fl Identity, *ternalhost*, *ticationmeth*
#OUTLOOK WEB APP (OWA)
Get-ExchangeServer | Get-OwaVirtualDirectory | fl Identity, *ternalurl*
#EXCHANGE WEB SERVICES (EWS)
Get-ExchangeServer | Get-WebServicesVirtualDirectory | fl Identity, *ternalurl*

