﻿
<#
Make Sure!

Schema Admins group membership
Exchange Organization Management role group membership
#>

#Run to prepare Schema (Active Directory)

<Virtual DVD drive letter>:\Setup.exe /IAcceptExchangeServerLicenseTerms /PrepareSchema


#Install Pre-Reqs
Install-WindowsFeature AS-HTTP-Activation, Desktop-Experience, NET-Framework-45-Features, RPC-over-HTTP-proxy, RSAT-Clustering, RSAT-Clustering-CmdInterface, RSAT-Clustering-Mgmt, RSAT-Clustering-PowerShell, Web-Mgmt-Console, WAS-Process-Model, Web-Asp-Net45, Web-Basic-Auth, Web-Client-Auth, Web-Digest-Auth, Web-Dir-Browsing, Web-Dyn-Compression, Web-Http-Errors, Web-Http-Logging, Web-Http-Redirect, Web-Http-Tracing, Web-ISAPI-Ext, Web-ISAPI-Filter, Web-Lgcy-Mgmt-Console, Web-Metabase, Web-Mgmt-Console, Web-Mgmt-Service, Web-Net-Ext45, Web-Request-Monitor, Web-Server, Web-Stat-Compression, Web-Static-Content, Web-Windows-Auth, Web-WMI, Windows-Identity-Foundation –Restart

Microsoft Unified Communications Managed API 4.0, Core Runtime 64-bit
http://go.microsoft.com/fwlink/p/?linkId=258269
NET Framework 4.5.2 
http://go.microsoft.com/fwlink/p/?LinkId=518380


#Run the install

Setup.exe /mode:Install /role:Mailbox /IAcceptExchangeServerLicenseTerms